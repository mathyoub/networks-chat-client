/*
*
*	Matthew Beall
*
*	Worked from code supplied by John Rodkey
*	Worked along side Drew Austin
*	Recieved help with multithreading and ncurses from Ryan Kleinburg
*
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <sys/select.h>
#include <sys/time.h>
#include <ncurses.h>
#include <curses.h>
#include <pthread.h>

#define SERVER "10.115.20.250"
#define PORT 49155
#define BUFSIZE 1024

//allows for colored text and reset to normal
#define RED   "\x1B[31m"
#define RESET "\x1B[0m"

int fd,len;

//window declaration
WINDOW * outwindow;
WINDOW * inwindow;
WINDOW * soutwindow;
WINDOW * sinwindow;


int connect2v4stream(char * srv, int port) {
	int ret,sockd;
	struct sockaddr_in sin;

	if ( (sockd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		printf(RED "ERROR: Error creating socket. errno = %d\n" RESET, errno);
		exit(errno);
	}
	if ( (ret = inet_pton(AF_INET, SERVER, &sin.sin_addr)) <= 0){
		printf(RED "ERROR: trouble converting using inet_pton. \
			return value = %d, errno = %d\n" RESET, ret, errno);
		exit(errno);
	}

	sin.sin_family = AF_INET;  //IPv4
	sin.sin_port = htons(PORT); //Convert port to network endian

	if ( (connect(sockd, (struct sockaddr *) &sin, sizeof(sin))) == -1){
		printf(RED "ERROR: trouble connecting to server. errno = %d\n" RESET, errno);
		exit(errno);
	}

	return sockd;
}

int sendout(int fd, char *msg){
	int ret;
	ret = send(fd, msg, strlen(msg), 0);
	if ( ret == 1 ){
		printf(RED "ERROR: trouble sending. errno = %d\n" RESET, errno);
		exit(errno);
	}

	return strlen(msg);
}

void recvandprint (int fd, char *buff){
	int ret;

	for(;;){
		buff = malloc(BUFSIZE+1);
		ret = recv(fd, buff, BUFSIZE, 0);
		if( ret == -1 ){
			if( errno == EAGAIN ){
				break;
			}else{
				printf(RED "ERROR: error receiving. errno = %d\n" RESET, errno);
				exit(errno);
			}
		}else if( ret == 0){
			exit(0);
		}else{
			buff[ret] = 0;
			
			cbreak();
			wprintw(inwindow, buff);
			wrefresh(inwindow);

		}

		free(buff);
	}
}

//write thread for multithreading
void* wthread (void* param){
	char *buffer, *origbuffer;
	int is_done = 0;
	while( ! is_done ){
		recvandprint(fd,buffer); //print any input

		len = BUFSIZE;
		buffer = malloc(len+1);
		origbuffer = buffer;
		
		//puts the output in the window and adds a newline character
		wgetstr(soutwindow, buffer);
		strcat(buffer, "\n");
		wrefresh(soutwindow);

		sendout(fd, buffer);

		is_done = (strcmp (buffer, "quit\n") == 0);
		free(origbuffer);
	}

}

//read thread for multithreading
void* readthread(void* param){
	for(;;){
		char* b;
		recvandprint(fd, b);
	}
}

//function that makes all the different windows
void windowMaker(){
	initscr();

	//hardcoded height and width
	int height = 30;
	int width = 50;

	//windows that show what is being received
	outwindow = newwin(height, width, 1, 1);//sets size
	inwindow = newwin(height - 2, width - 2, 2, 2);
	refresh();

	scrollok(outwindow, TRUE);//allows scrolling
	scrollok(inwindow, TRUE);

	wrefresh(outwindow);
	wrefresh(inwindow);
	refresh();

	box(outwindow,0,0);
	wrefresh(inwindow);

	//windows that show what is being typed and has been sent
	soutwindow = newwin(height, width, 1, width+1);
	sinwindow = newwin(height - 2, width - 2, 2, width+2);
	refresh();

	scrollok(soutwindow, TRUE);
	scrollok(sinwindow, TRUE);

	wrefresh(soutwindow);
	wrefresh(sinwindow);
	refresh();

	box(soutwindow,0,0);
	wrefresh(sinwindow);

}



int main(int argc, char * argv[]) {
	char *name, *buffer, *origbuffer;
	struct timeval timev;

	pthread_t multiRead;
	pthread_t multiSend;

	fd = connect2v4stream( SERVER, PORT);

	//Setup recv timeout for .5 seconds
	timev.tv_sec = 0;
	timev.tv_usec = 1000 * 500;
	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));

	//Set name based on arguments
	if(argc < 2 ){
		printf(RED "Usage: chat-client <screenname\n" RESET);
		exit(1);
	}

	name = argv[1];
	len = strlen(name);
	name[len] = '\n'; //insert \n
	name[len+1] = '\0'; //reterminate the string
	sendout(fd, name);

	initscr();//initialize threads
	windowMaker();//makes all the windows

	//multithreading in actions
	pthread_create(&multiRead, NULL, readthread, NULL);
	pthread_create(&multiSend, NULL, wthread, NULL);

	pthread_join(multiRead, NULL);
	pthread_join(multiSend, NULL);

	endwin();//ends the windows

}




